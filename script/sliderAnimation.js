const customerHidden = document.querySelectorAll('.feedback_hidden');
const feedbackHidden = document.querySelectorAll('.slider_hidden');
const feedbackPhoto = Array.from(document.querySelectorAll('.nav_slider'));
const prevSlide = document.querySelectorAll('.left');
const nextSlide = document.querySelectorAll('.right');

const applyHiddenClass = (cls, length = cls.length, count = 0) => {
    for (let i = count; i < length; i++) {
        cls[i].style.display = 'none';
    }
};
applyHiddenClass(customerHidden);
applyHiddenClass(feedbackHidden);

function showReview() {
    const customerReview = Array.from(document.querySelectorAll('.feedback'));
    customerReview.forEach((value) => value.style.display = 'none');
    const customerData = this.getAttribute('data-customer');
    const reviewData = customerReview.filter((value) => {
        return value.getAttribute('data-customer') === customerData;
    });
    reviewData[0].style.display = '';
    feedbackPhoto.forEach((value) => value.classList.remove('slider_animation'));
    this.classList.add('slider_animation');
}

const prevReview = () => {
    const currentImageIndex = feedbackPhoto.findIndex((items) => items.classList.contains('slider_animation'));
    const currentImage = document.querySelectorAll('.slider_animation');
    let prevImageIndex = currentImageIndex - 1;
    let prevImage = feedbackPhoto[prevImageIndex];
    const customerReview = Array.from(document.querySelectorAll('.feedback'));
    customerReview.forEach((items) => items.style.display = 'none');

    if (currentImageIndex === 0) {
        feedbackPhoto.forEach((items) => items.style.display = '');
        prevImage = feedbackPhoto[feedbackPhoto.length - 1];
        prevImageIndex = feedbackPhoto.length - 1;
        currentImage[currentImageIndex].style.display = 'none';
    }
    if (prevImage.style.display === 'none') {
        prevImage.style.display = '';
        feedbackPhoto[currentImageIndex + 3].style.display = 'none';
    }
    currentImage[0].classList.remove('slider_animation');
    prevImage.classList.add('slider_animate');
    customerReview[prevImageIndex].style.display = '';
    customerReview[prevImageIndex].classList.add('animate__slideInRight');
};

const nextReview = () => {
    const currentImageIndex = feedbackPhoto.findIndex((items) => items.classList.contains('slider_animation'));
    const currentImage = document.querySelectorAll('.slider_animation');
    let nextImageIndex = currentImageIndex + 1;
    let nextImage = feedbackPhoto[nextImageIndex];
    const customerReview = Array.from(document.querySelectorAll('.feedback'));

    customerReview.forEach((items) => items.style.display = 'none');

    if (currentImageIndex === (feedbackPhoto.length - 1)) {
        feedbackPhoto.forEach((items) => items.style.display = '');
        nextImage = feedbackPhoto[0];
        nextImageIndex = 0;
        currentImage[0].style.display = 'none';
    }
    if (nextImage.style.display === 'none') {
        nextImage.style.display = '';
        feedbackPhoto[currentImageIndex - 3].style.display = 'none';
    }
    currentImage[0].classList.remove('slider_animation');
    nextImage.classList.add('slider_animation');
    customerReview[nextImageIndex].style.display = '';
    customerReview[nextImageIndex].classList.add('animate__slideInLeft');
};

const thumbnail = document.querySelectorAll('.nav_slider');

thumbnail.forEach(function(item){
    item.addEventListener('click', showReview);
});

prevSlide[0].addEventListener('click', prevReview);
nextSlide[0].addEventListener('click', nextReview);
