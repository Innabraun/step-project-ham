$("#tabs").click((e) => {
    $(".our-services__menu_list.active").removeClass("active");
    $(e.target).addClass("active");
    $("#tabsContent li").removeClass("active");
    $(`#tabsContent li[data-content=${e.target.dataset.content}]`).addClass("active");
});

/////////////////

$(document).ready(() => {
    $('.load_more_btn').click((event) => {
        setTimeout(() => {
            $('.load_more_btn').hide();
            $('.amazing_image_item.hidden').slice(0, 12).removeClass('hidden').show();
            if (!$('.amazing_image_item').hasClass('hidden')) {
                $('.load_btn').remove();
            }
        }, 1000);
    });
});

/////////////////

$(document).ready(() => {
    let loadMore = $('.load_btn');

    $('.all').click((event) => {
        event.preventDefault();
        $('.amazing_image_item').hide();
        $('.amazing_image_item:not(.hidden)').show();
        loadMore.show();
    });

    $('.graphic').click((event) => {
        event.preventDefault();
        $('.amazing_image_item').filter('li[data-category*="graphic"]').show();
        $('.amazing_image_item').filter(':not(li[data-category*="graphic"])').hide();
        loadMore.hide();
    });

    $('.web').click((event) => {
        event.preventDefault();
        $('.amazing_image_item').filter('li[data-category*="web"]').show();
        $('.amazing_image_item').filter(':not(li[data-category*="web"])').hide();
        loadMore.hide();
    });

    $('.landing').click((event) => {
        event.preventDefault();
        $('.amazing_image_item').filter('li[data-category*="landing"]').show();
        $('.amazing_image_item').filter(':not(li[data-category*="landing"])').hide();
        loadMore.hide();
    });

    $('.wordpress').click((event) => {
        event.preventDefault();
        $('.amazing_image_item').filter('li[data-category*="wordpress"]').show();
        $('.amazing_image_item').filter(':not(li[data-category*="wordpress"])').hide();
        loadMore.hide();
    })
});

//////////////////////////


